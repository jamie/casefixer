<?php

declare(strict_types=1);

/**
 * @copyright Copyright (c) 2021, May First Movement Technology.
 *
 * @author Jamie McClelland <jamie@mayfirst.coop>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */

namespace OCA\CaseFixer;
use OCP\IDBConnection;

class Fixer {

  protected $dbConnection;
  protected $conn;

  // Some columns contain user names but aren't exclusively usernames
  // These are collected here for future reference but are not used.
  public $possiblyIncludeColumnTables = [
    'oc_mounts.mount_point',
    'oc_storages.id',
  ];

  // Some columns contain a username within them, along with other
  // text. Our script won't detect them because you can't join them
  // against the user table, but they still need to be included.
  public $mixedTextColumns = [
    'principaluri',
  ];

  // json field types behave differently.
  public $jsonColumns = [
    'access_json',
  ];
  // Some columns may look like user columns but they aren't.
  public $excludeColumnTables = [
    'oc_files_trash.id',
    'oc_accounts_data.value',
    'oc_calendars.displayname',
    'oc_calendarobjects_props.value',
    'oc_activity.app', 
    'oc_activity.type', 
    'oc_activity.object_type', 
    'oc_addressbooks.displayname',
    'oc_cards_properties.value',
    'oc_users_external.displayname',
  ];

  // Some columns take a long time to scan and we know we want them.
  public $includeColumns = [
    'oc_activity.affecteduser',
    'oc_acivity.user'
  ];
  /**
   * CaseFixer constructor
   *
   * @param IDBConnection $dbConnection.
   */
  public function __construct(IDBConnection $dbConnection) {
    $this->dbConnection = $dbConnection;
    $this->conn = $this->dbConnection->getQueryBuilder()->getConnection();
    $this->createReferenceUserTable();
  }

  /*
   * Create a table of all lowercased user names
   *
   * We'll use this table to figure out if any columns contain usernames.
   */
  public function createReferenceUserTable() {
    $dsql = "DROP TABLE IF EXISTS tmp_user";
    $csql = "CREATE TEMP TABLE tmp_user AS SELECT DISTINCT(LOWER(uid)) AS uid FROM oc_users_external";
    $stmt = $this->conn->prepare($dsql);
    $stmt->execute();
    $stmt = $this->conn->prepare($csql);
    $stmt->execute();
  }
  public function isCharColumn($table, $column) {
    $sql = "SELECT data_type 
      FROM information_schema.columns 
      WHERE table_name = '$table' AND column_name = '$column'";
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch();
    if (preg_match('/^character/', $row['data_type'])) {
      return TRUE;
    }
    return FALSE;
  }

  public function getColumnList() {
    $ret = [];
    // Try to get a distinct list of tables and columns that have an index
    // and are char columns. These are all candidates for lowercasing usernames.
    $sql = "SELECT DISTINCT table_name, column_name 
      FROM pg_index i JOIN pg_attribute a 
        ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey) 
      JOIN information_schema.columns c 
        ON a.attname = c.column_name 
      WHERE 
        table_schema NOT IN ('pg_catalog', 'information_schema')
        AND data_type ~ '^char'";
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
    
    while ($row = $stmt->fetch()) {
      $column = $row['column_name'];
      $table = $row['table_name'];

      // Exclude known non-user name columns.
      if (in_array("${table}.${column}", $this->excludeColumnTables)) {
        continue;
      }
      // Check for upper case values
      if ($this->upperCaseValueCount($table, $column) == 0) {
        // No upper case values, so skip it. Even if it contains usernames, there
        // is nothing for us to do with this column.
        continue;
      }

      // principaluri columns don't match against the user table but we must
      // lowercase them.
      if (!in_array($column, $this->mixedTextColumns) && $this->userNameJoinCount($table, $column) == 0) {
        continue;
      }
      $ret[] = [ 
        'table' => addslashes($table),
        'column' => addslashes($column)
      ];
    }
    return $ret;
  }

  public function countResultsFromQuery($sql) {
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch();
    if(empty($row['count'])) {
      return 0;
    }
    return $row['count'];

  }
  /*
   * Try to join this table/column with our username list.
   *
   */
  public function userNameJoinCount($table, $column) {
    if (in_array("${table}.${column}", $this->includeColumns)) {
      return 999999;
    }
    $sql = "SELECT COUNT(*) FROM $table t1 JOIN tmp_user t2 ON LOWER(t1.$column) = LOWER(t2.uid)";
    return $this->countResultsFromQuery($sql);
  }

  /*
   * Count rows with a single upper case letter
   */
  public function upperCaseValueCount($table, $column) {
    if (in_array("${table}.${column}", $this->includeColumns)) {
      return 999999;
    }
    // It takes a long time to scan th
    if (in_array($column, $this->mixedTextColumns)) {
      $sql = "SELECT COUNT(*) AS count 
        FROM $table WHERE $column ~ '[A-Z]'";
    }
    elseif (in_array($column, $this->jsonColumns)) {
      $sql = "SELECT COUNT(*) AS count 
        FROM $table WHERE $column::text ~ '[A-Z]'";
    }
    else {
      $sql = "SELECT COUNT(*) AS count 
        FROM $table t1 JOIN tmp_user t2 
          ON LOWER(t1.$column) = LOWER(t2.uid) 
      WHERE t1.$column ~ '[A-Z]'";
    }
    return $this->countResultsFromQuery($sql);
  }

  public function getSample($table, $column) {
    $sql = "SELECT t1.$column AS sample 
      FROM $table t1 ";
    
    if (!in_array($column, $this->mixedTextColumns)) {
      $sql .= "JOIN tmp_user t2 ON LOWER(t1.$column) = LOWER(t2.uid) ";
    }

    $sql .=  "WHERE t1.$column ~ '[A-Z]' LIMIT 5";
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
    $ret = [];
    while($row = $stmt->fetch()) {
      $ret[] = $row['sample'];
    }
    return $ret;
  }

  /*
   * Count rows with duplicate (case insensitive) values - unique
   * 
   * Take into account other unique columns.
   */
  public function multipleKeyDupeCount($table, $column, $unique_key_columns) {
    $joins = [];
    foreach($unique_key_columns as $ucolumn) {
      if ($column == $ucolumn) {
        continue;
      }
      $joins[] = "t1.${ucolumn} = t2.${ucolumn}";
    }
    $join_str = '';
    if (count($joins) > 0) {
      $join_str = ' AND ' . implode(' AND ', $joins);
    }
    $sql = "SELECT COUNT(*) AS count 
      FROM $table t1 JOIN $table t2 
        ON LOWER(t1.${column}) = LOWER(t2.${column}) AND t1.${column} != t2.${column} 
          $join_str
      HAVING COUNT(*) > 1";
    return $this->countResultsFromQuery($sql);
    
  }

  /*
   * Count rows with duplicate (case insensitive) values
   * 
   * In other words... if I lower case all the values in this
   * column, will I have a problem?
   */
  public function singleKeyDupeCount($table, $column) {
    $sql = "SELECT COUNT(*) AS count 
      FROM $table t1 JOIN $table t2 
        ON LOWER(t1.${column}) = LOWER(t2.${column}) AND t1.${column} != t2.${column} 
      HAVING COUNT(*) > 1";
     return $this->countResultsFromQuery($sql);
  }

  /*
   * Return unique columns
   *
   * If this column is part of a unique index, return all
   * the columns that, together, make up the unique
   * index.
   */
  public function getUniqueKeyColumns($table, $column) {
    // There must be a better way.
    $sql = "
      SELECT 
        ARRAY( 
          SELECT pg_get_indexdef(idx.indexrelid, k + 1, true) 
            FROM generate_subscripts(idx.indkey, 1) as k ORDER BY k
        ) as indkey_names 
      FROM pg_index as idx 
        JOIN pg_class as i ON i.oid = idx.indexrelid 
        JOIN pg_am as am ON i.relam = am.oid 
      WHERE indrelid = '$table'::regclass AND indisunique IS TRUE"; 
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
    while($row = $stmt->fetch()) {
      $columns = trim($row['indkey_names'], '{}');
      $columns = explode(',', $columns);
      if (in_array($column, $columns)) {
  
        return $columns;
      }
    }
    return [];
  }

  /*
  * Return TRUE if column is a primary key.
  */
  public function isPrimaryKey($table, $column) {
    $table = addslashes($table);
    $column = addslashes($column);
    $sql = "SELECT COUNT(*) AS count 
      FROM pg_index i 
        JOIN pg_attribute a ON a.attrelid = i.indrelid
          AND a.attnum = ANY(i.indkey)
      WHERE i.indrelid = '$table'::regclass
        AND i.indisprimary AND attname = '$column'";
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch();
    if ($row['count'] == 0) {
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Run this function when there are no constraints
   */
  public function lowerCaseColumnValues($table, $column) {
    if (in_array($column, $this->mixedTextColumns)) {
      // If this is a mixed text field we can't join on the known user table
      // to limit to known users. So, we just go for it.
      $sql = "UPDATE $table SET \"$column\" = LOWER($column) WHERE \"$column\" ~ '[A-Z]'";
    }
    elseif (in_array($column, $this->jsonColumns)) {
      // JSON columns need special love.
      $sql = "UPDATE $table SET \"$column\" = LOWER($column::text)::json WHERE \"$column\"::text ~ '[A-Z]'";
    }
    else {
      // Just to be safe, don't lower case a value that is not a known user. A few
      // fields have both users and other text in these columns
      $sql = "UPDATE $table SET \"$column\" = LOWER($column) WHERE \"$column\" ~ '[A-Z]'
        AND LOWER($column) IN (SELECT uid FROM tmp_user)";
    }
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
  }

  /**
   * Use this function to lower case tables when the column must be singularly unique.
   */
  public function lowerCaseColumnValuesWhenPossibleSingle($table, $column) {
    // Lowercase the column when it has a matching column in the known user table
    // and when there is no lowercase version already existing in the table.
    if (in_array($column, $this->mixedTextColumns)) {
      $sql = "SELECT DISTINCT \"$column\" AS username FROM $table";
    }
    else {
      $sql = "SELECT DISTINCT t1.{$column} AS username FROM $table t1 JOIN tmp_user t2 ON LOWER(t1.${column}) = t2.uid";
    }
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();

    // Keep track of all users that we've seen so we don't run twice on the same user.
    $seen = [];
    while($row = $stmt->fetch()) {
      $user = $row['username']; 
      $luser = strtolower($user);
      if ($user === $luser) {
        // This is already lower cased, nothing to do.
        continue;
      }
      if (in_array($luser, $seen)) {
        // We've already handled this username.
        continue;
      }
      
      // See if there is a lowercase version already in the database.
      $sql = "SELECT COUNT(*) AS count FROM $table WHERE \"$column\" = :luser";
      $lcstmt = $this->conn->prepare($sql);
      $lcstmt->execute(['luser' => $luser]);
      $lcrow = $lcstmt->fetch();
      if ($lcrow['count'] > 0) {
        // We already have a lowercase version in there, so skip.
        $seen[] = $luser;
        continue;
      }
      
      // Convert.
      $sql = "UPDATE $table SET \"$column\" = :luser WHERE \"$column\" = :user";
      $ustmt = $this->conn->prepare($sql);
      $params = [
        'luser' => $luser,
        'user' => $user
      ];
      $ustmt->execute($params);
      $seen[] = strtolower($user);
    }

    
  }
  /**
   * Use this function to lower case tables when the column shares one unique index
   * with other columns
   */
  public function lowerCaseColumnValuesWhenPossibleMultiple($table, $column, $unique_key_columns) {
    // Lowercase the column when it has a matching column in the known user
    // table and when there is no lowercase version WITH GIVEN INDICES already
    // existing in the table.
    
    // Select a distinct list of all the unique index combinations.
    $sanitized_unique_key_columns = [];
    foreach($unique_key_columns as $col) {
      if ($col == 'uid') {
        $sanitized_unique_key_columns[] = "t1.uid";
      }
      else {
        $sanitized_unique_key_columns[] = $col;
      }
    }
    $fields = implode(',', $sanitized_unique_key_columns);

    if (in_array($column, $this->mixedTextColumns)) {
      $sql = "SELECT $fields, CONCAT_WS(',', $fields) AS ind 
      FROM $table t1 WHERE $column LIKE 'principals/users/%'";
    }
    else {
      $sql = "SELECT $fields, CONCAT_WS(',', $fields) AS ind 
      FROM $table t1 JOIN tmp_user t2 ON LOWER(t1.${column}) = t2.uid";
    }
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();

    // Keep track of all indexed fields that we've seen so we don't run twice on the same combo.
    $seen = [];
    while($row = $stmt->fetch()) {
      $ind = $row['ind']; 
      $user = $row[$column];
      $luser = strtolower($user);
      if ($user === $luser) {
        // This is already lower cased, nothing to do.
        continue;
      }

      // Create a lowercased version of the index which we will use to ensure we don't
      // run twice.
      $lind = str_replace($user, $luser, $ind);
      if (in_array($lind, $seen)) {
        // We've already handled this combo.
        continue;
      }
      
      // See if there is a lowercase version already in the database with the same
      // indices. If there is we don't want to run.
      $where_clauses = [];
      $params = [
        'luser' => $luser,
      ];

      // Build the WHERE clause with the indices.
      foreach($unique_key_columns as $col) {
        if ($col == $column) {
          // Skip the main column, that is included separately.
          continue;
        }
        // Set the WHERE statement.
        $value_token = ":${col}_value";
        $where_clauses[] = "\"$col\" = $value_token";

        // Fill in the parameters.
        $params[$value_token] = $row[$col];
      }
      $sql = "SELECT COUNT(*) AS count FROM $table WHERE \"$column\" = :luser AND " . 
        implode(' AND ', $where_clauses);

      $istmt = $this->conn->prepare($sql);
      $istmt->execute($params);
      $irow = $istmt->fetch();
      if ($irow['count'] > 0) {
        // We already have a lowercase version in there, so skip.
        $seen[] = $lind;
        continue;
      }
      
      // Convert.
      $sql = "UPDATE $table SET \"$column\" = :luser WHERE \"$column\" = :user
        AND " . implode(' AND ', $where_clauses);
      $ustmt = $this->conn->prepare($sql);
      // Reuse params from last statement.
      $params['user'] = $user;
      $ustmt->execute($params);
      $seen[] = $lind;
    }
  }

  public function fixAddressBooks() {
    // Find all address books with upper case principal URIs.
    $sql = "SELECT id, principaluri FROM oc_addressbooks WHERE principaluri ~ '[A-Z]'";
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();

    // $unique_lower_case_uris contains a set of distinct uris for all users
    // with at least one mixed case principaluri. There is just
    // one for each user and it's stored in lowercase.
    $unique_lower_case_uris = [];
    while ($row = $stmt->fetch()) {
      $id = $row['id'];
      $principaluri = $row['principaluri'];
      if (!in_array(strtolower($principaluri), $unique_lower_case_uris)) {
        $unique_lower_case_uris[$id] = strtolower($principaluri);
      }
    }

    // Make sure there is one lower case version of each principaluri in the
    // oc_addressbooks table. 
    foreach($unique_lower_case_uris as $id => $principaluri) {
      $sql = "SELECT id FROM oc_addressbooks WHERE principaluri = :principaluri ";
      $stmt = $this->conn->prepare($sql);
      $stmt->execute(['principaluri' => $principaluri]);
      $row = $stmt->fetch();
      $found_id = NULL;
      if (isset($row['id'])) {
        $found_id = $row['id'];
      }
      if (empty($found_id)) {
        // There is no match, so we will update the one we have to make it lower case
        // in the table. This will become the canonical address book for this user.
        $sql = "UPDATE oc_addressbooks SET principaluri = :principaluri WHERE id = :id";
        $ustmt = $this->conn->prepare($sql);
        $ustmt->execute(['principaluri' => $principaluri, 'id' => $id]);
      }
      else {
        // There is a record in the database. If it has a different id then the one we have
        // we should update our array to use that id so it becomes the canonical address book
        // for tihs user.
        if ($found_id != $id) {
          unset($unique_lower_case_uris[$id]);
          $unique_lower_case_uris[$found_id] = $principaluri;
        } 
      }
    }

    // For each principaluri variation in oc_cards, we have to update it to use
    // the canonical address book instead.
    foreach($unique_lower_case_uris as $addressbookid => $principaluri) {
      $sql = "SELECT oc_cards.id AS id FROM oc_addressbooks JOIN oc_cards ON
       oc_addressbooks.id = oc_cards.addressbookid WHERE LOWER(principaluri) = :principaluri
       AND addressbookid != :addressbookid";
      $stmt = $this->conn->prepare($sql);
      $stmt->execute(['principaluri' => $principaluri, 'addressbookid' => $addressbookid]);
      while($row = $stmt->fetch()) {
        $sql = "UPDATE oc_cards SET addressbookid = :addressbookid WHERE id = :id";
        $ustmt = $this->conn->prepare($sql);
        $ustmt->execute(['id' => $row['id'], 'addressbookid' => $addressbookid]);
      }
    }
  }
}
