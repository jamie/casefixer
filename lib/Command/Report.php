<?php

declare(strict_types=1);

/**
 * @copyright Copyright (c) 2021, May First Movement Technology.
 *
 * @author Jamie McClelland <jamie@mayfirst.coop>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */

namespace OCA\CaseFixer\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use OCP\IDBConnection;
use OCA\CaseFixer\Fixer;

class Report extends Command {

  protected $caseFixer;

  /**
   * Report constructor
   *
   * @param IDBConnection $dbConnection.
   */
  public function __construct(IDBConnection $dbConnection) {
    parent::__construct();
    $this->caseFixer = new Fixer($dbConnection);
  }

  protected function configure() {
    $this->setName('casefixer:report')
      ->setDescription('Report on state of mixed case logins.')
      ->addArgument(
        'column',
        InputArgument::OPTIONAL,
        'The column to run on in the format table.column'
      );
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {
    $inputColumn = $input->getArgument('column');
    if (empty($inputColumn)) {
      $output->writeln("Generating column list. This will take a while.");
      $columns = $this->caseFixer->getColumnList();
      $output->writeln("DONE Generating column list.");
    }
    else {
      $columns = [];
      $inputColumnParts = explode('.', $inputColumn); 
      if (count($inputColumnParts) != 2) {
        $output->writeln('<error>', "Could not parse the column. Please use table.column format.");
        return 1;
      }
      $table = $inputColumnParts[0];
      $column = $inputColumnParts[1];
      $output->writeln("Using $column in $table.");
      $columns[] = [
        'table' => $table, 
        'column' => $column
      ];
    }
    foreach($columns as $values) {
      $column = $values['column'];
      $table = $values['table'];

      $is_primary_key = $this->caseFixer->isPrimaryKey($table, $column);
      // Special exceptions. 
      if ($table == 'oc_circle_members' && $column == 'user_id') {
        $func = 'lowerCaseColumnValues';
      }
      elseif ($table == 'oc_dav_shares' && $column == 'principaluri') {
        $func = 'lowerCaseColumnValuesWhenPossibleMultiple';
      }
      elseif ($is_primary_key) {
        $dupe_value_count = $this->caseFixer->singleKeyDupeCount($table, $column);
        $key = 'P';
        
        if ($dupe_value_count == 0) {
          $func = 'lowerCaseColumnValues';
        }
        else {
          $func = 'lowerCaseColumnValuesWhenPossibleSingle';
        }
      }
      else {
        $unique_key_columns = $this->caseFixer->getUniqueKeyColumns($table, $column);
        $unique_key_columns_count = count($unique_key_columns);
        if ($unique_key_columns_count > 0) {
          $key = 'U(' . implode(',', $unique_key_columns) . ')';
          if ($unique_key_columns_count == 1) {
            // This is effectively like a primary key. Use the simpler methods.
            $dupe_value_count = $this->caseFixer->singleKeyDupeCount($table, $column, $unique_key_columns);
            if ($dupe_value_count == 0) {
              $func = 'lowerCaseColumnValues';
            }
            else {
              $func = 'lowerCaseColumnValuesWhenPossibleSingle';
            }
          }
          else {
            $dupe_value_count = $this->caseFixer->multipleKeyDupeCount($table, $column, $unique_key_columns);
            if ($dupe_value_count == 0) {
              $func = 'lowerCaseColumnValues';
            }
            else {
              $func = 'lowerCaseColumnValuesWhenPossibleMultiple';
            }
          }
        }
        else {
          $key = 'NC'; // no constraints
          $func = 'lowerCaseColumnValues';
        }
      }
      $sample = implode(',', $this->caseFixer->getSample($table, $column));
      $output->writeln("$table:$column:$key:$dupe_value_count:$func:$sample");
    }
    return 0;
  }
}
